# Quarto web site

Template for producing [quarto web sites](https://quarto.org/docs/websites/).


## How to use

- Fork this project and make it independent:

    `Settings > General > Advanced > remove fork relationship`


- Configure the web site options in `_quarto.yml`.


- Pushing to the repository will compile and publish the web site on line in GitLab pages.

- The repository uses [`renv`](https://rstudio.github.io/renv/) for keeping track of versions of R and R-packages used, and build reproducibly.